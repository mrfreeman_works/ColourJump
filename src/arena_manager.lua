local S = minetest.get_translator("colour_jump")

local function contains() end
local function print_timer() end
local function set_platform() end
local function set_platform_air() end
local function random_blocks() end

local takenNumbers = {}
local newPosPlatformsList = {}
local checker = 0
local newPosPlatform = {}

local active_arenas = {}

arena_lib.on_load("colour_jump", function(arena)
    arena.timer_current = arena.timer_initial_duration

    for prop_name, prop in pairs(arena) do
        if string.find(prop_name, "arenaCol_") and prop.isActive == true then
            prop.y = arena.y
            set_platform(prop)
            table.insert(arena.items, prop.id)
        end
    end

    for pl_name, _ in pairs(arena.players) do
        minetest.chat_send_player(pl_name, S("The minigame will start in a few seconds!"))
        minetest.chat_send_player(pl_name, S("To win, you have to be the last one standing! Reach the correct platform when it will be show on your screen...GOOD LUCK!"))
    end

    table.insert(active_arenas, arena.name)
end)

arena_lib.on_end("colour_jump", function(arena)
    for a=1, #active_arenas do
        if active_arenas[a] == arena.name then
            table.remove(active_arenas, a)
            return
        end
    end
end)

arena_lib.on_celebration('colour_jump', function(arena, winner_name)
    arena_lib.HUD_send_msg_all("title", arena, S('The game is over!'), 2 ,'colour_jump_win',0xAEAE00)
    arena.items = {}
    arena.list_values = {}
    for pl_name, _ in pairs(arena.players) do
        local player = minetest.get_player_by_name(pl_name)
        if colour_jump.HUD[pl_name] then
            player:hud_remove(colour_jump.HUD[pl_name].scores)
            player:hud_remove(colour_jump.HUD_BACKGROUND[pl_name].background)
            colour_jump.HUD[pl_name] = nil
        end

        minetest.after(3, function()
            local highscore = {[1]="",[2]=0}
            for pll_name, _ in pairs(arena.players) do
                if arena.rounds_counter_temp > highscore[2] then
                    highscore = {pll_name, arena.rounds_counter_temp}
                end
            end

            if colour_jump.HUD[pl_name] then
                player:hud_remove(colour_jump.HUD[pl_name].scores)
                player:hud_remove(colour_jump.HUD_BACKGROUND[pl_name].background)
                colour_jump.HUD[pl_name] = nil
            end
        end, 'Done')
    end
end)


arena_lib.on_quit('colour_jump', function(arena, pl_name, is_forced)
    local player = minetest.get_player_by_name(pl_name)
    if colour_jump.HUD[pl_name] then
        player:hud_remove(colour_jump.HUD[pl_name].scores)
        player:hud_remove(colour_jump.HUD_BACKGROUND[pl_name].background)
        colour_jump.HUD[pl_name] = nil
    end
end)

arena_lib.on_time_tick("colour_jump", function(arena)
    local stringOfRoundHUD = S('Lap: @1', arena.rounds_counter_temp)

    if ((arena.current_time % 10) == 0 and not arena.in_celebration) then
        arena.rounds_counter_temp = arena.rounds_counter_temp + 1
        local right_platform_id = math.random(1, arena.platforms_amount)
        random_blocks(arena)
        if arena.timer_current > arena.timer_min_duration then
            arena.timer_current = arena.timer_current - arena.timer_decrease_value
        else
            arena.timer_current = arena.timer_min_duration
        end
        for _, props in pairs(newPosPlatformsList) do
            if props.id == tostring(arena.items[right_platform_id]) then
                arena_lib.HUD_send_msg_all("title", arena, tostring(S(arena.items[right_platform_id])) , 3, nil, tonumber(props.hexColor))
            end
        end
        arena.seconds_left = arena.timer_current

        minetest.after(arena.timer_current, function()
            for _, props in pairs(newPosPlatformsList) do
                if props.id ~= tostring(arena.items[right_platform_id]) then
                    set_platform_air(props)
                end
            end
            newPosPlatformsList = {}
        end, 'Done')
    end

    local countPeopleFallen = 0

    for pl_name in pairs(arena.players) do
        local player = minetest.get_player_by_name(pl_name)
        if player:get_pos().y < arena.y -4 then
                countPeopleFallen = countPeopleFallen + 1
        end
    end

    if (countPeopleFallen == arena.players_amount) and arena.players_amount > 1 then
        arena_lib.HUD_send_msg_all("title", arena, S('All the players fallen down! Nobody won'), 3, nil, "0xB6D53C")
        for pl_name in pairs(arena.players) do
                local player = minetest.get_player_by_name(pl_name)
                if colour_jump.HUD[pl_name] then
                        player:hud_remove(colour_jump.HUD[pl_name].scores)
                        player:hud_remove(colour_jump.HUD_BACKGROUND[pl_name].background)
                        colour_jump.HUD[pl_name] = nil
                        arena_lib.remove_player_from_arena( pl_name , 1 )
                end
        end
        -- arena_lib.force_arena_ending('colour_jump', arena, 'ColourJump')
    else
        -- TODO: move HUD into a separate file
        for pl_name in pairs(arena.players) do
            local player = minetest.get_player_by_name(pl_name)
            if not arena.in_celebration then
                if not colour_jump.HUD[pl_name] then
                    local new_hud_image = {}
                    new_hud_image.background = player:hud_add({
                    hud_elem_type = "image",
                    position  = {x = 1, y = 0},
                    offset = {x = -179, y = 32},
                    name = "colour_jump_background",
                    text = "HUD_colour_jump_round_counter.png",
                    alignment = { x = 1.0},
                    scale     = { x = 1.15, y = 1.15},
                    z_index = 100
                    })
                    colour_jump.HUD_BACKGROUND[pl_name] = new_hud_image

                    local new_hud = {}
                    new_hud.scores = player:hud_add({
                    hud_elem_type = "text",
                    position  = {x = 1, y = 0},
                    offset = {x = -155, y = 32},
                    alignment = {x = 1.0},
                    scale = {x = 2, y = 2},
                    name = "colour_jump_highscores",
                    text = S('Lap: @1', arena.rounds_counter_temp),
                    z_index = 100,
                    number    = "0xFFFFFF"
                    })
                    colour_jump.HUD[pl_name] = new_hud

                else
                    local idText = colour_jump.HUD[pl_name].scores
                    player:hud_change(idText, "text", stringOfRoundHUD)
                    local idBackground = colour_jump.HUD_BACKGROUND[pl_name].background
                    player:hud_change(idBackground, "text", "HUD_colour_jump_round_counter.png")
                end
            end

            if player:get_pos().y < arena.y -4 then
                -- reset players pos if they fall down before the first colour appears
                -- (but don't if they're dead, e.g. melt in lava, so they will be
                -- eliminated the second right after)
                if arena.current_time <= arena.timer_initial_duration and player:get_hp() > 0 then
                    player:set_pos(arena_lib.get_random_spawner(arena, nil, true).pos)
                else
                    if colour_jump.HUD[pl_name] then
                        player:hud_remove(colour_jump.HUD[pl_name].scores)
                        player:hud_remove(colour_jump.HUD_BACKGROUND[pl_name].background)
                        colour_jump.HUD[pl_name] = nil
                    end

                    arena_lib.remove_player_from_arena( pl_name , 1 )
                    return
                end
            end
        end
    end

end)



--- LOCAL FUNCTIONS

-- function created to check if a value is inside a list. Used inside the function above for the random switch positions ( fn random_blocks() )
function contains(table, val)
    for i=1,#table do
       if table[i] == val then
          return true
       end
    end
    return false
end

function print_timer(arena)
    local time = arena.seconds_left
    local round = arena.rounds_counter_temp
    if round ~= 0 and time > 0 then
        local time_flat = math.ceil(time)
        if arena.last_hud_seconds ~= time_flat then
            arena_lib.HUD_send_msg_all("hotbar", arena, S('The platforms disappear in: @1 SECS!', time_flat), 1.5, nil, 0xFFFFFF)
            arena.last_hud_seconds = time_flat
        end
    else
        if arena.last_hud_seconds ~= -1 then
            arena_lib.HUD_hide("hotbar", arena)
            arena.last_hud_seconds = -1
        end
    end
end

function set_platform(colour)
    local poss = {}
    for i = -1,1 do
        for k = -1,1 do
            table.insert(poss, vector.new(colour.x-i,colour.y,colour.z-k))
        end
    end
    minetest.bulk_set_node(poss, {name=colour.name})
end

function set_platform_air(colour)
    local poss = {}
    for i = -1,1 do
        for k = -1,1 do
            table.insert(poss, vector.new(colour.x-i,colour.y,colour.z-k))
        end
    end
    minetest.bulk_set_node(poss, {name="air"})
end

function random_blocks(arena)

  local list_values = arena.list_values

    for prop_nome,prop in pairs(arena) do
        if string.find(prop_nome, "arenaCol_") and prop.isActive == true then
            local values = {}
            values = {x = tonumber(prop.x), y = tonumber(arena.y), z = tonumber(prop.z), id = tostring(prop.id), name = tostring(prop.name), hexColor = tostring(prop.hexColor)}
            table.insert(list_values, values)
        end
    end

    local platforms_amount = arena.platforms_amount

    takenNumbers = {}
    newPosPlatformsList = {}
    checker = math.random(1, platforms_amount)
    for i=1, platforms_amount do
        while (contains(takenNumbers, checker)) do
            checker = math.random(1, platforms_amount)
            if not contains(takenNumbers, checker) then break end
        end

        if  (not contains(takenNumbers, checker)) then
            newPosPlatform = {}
            newPosPlatform.x = list_values[i].x
            newPosPlatform.y = arena.y
            newPosPlatform.z = list_values[i].z
            newPosPlatform.id = list_values[checker].id
            newPosPlatform.name = list_values[checker].name
            newPosPlatform.isActive = list_values[checker].isActive
            newPosPlatform.hexColor = list_values[checker].hexColor
            set_platform(newPosPlatform)
        end
        table.insert(takenNumbers, checker)
        table.insert(newPosPlatformsList, newPosPlatform)
    end
    for prop_nome,prop in pairs(arena) do
        if string.find(prop_nome, "arenaCol_") and prop.isActive == true then
            if prop_nome == newPosPlatform.id then
                prop = newPosPlatform
            end
        end
    end

    for i=1, #takenNumbers do
        takenNumbers[i] = nil
    end
end

local dtime_precision = 0.1
local dtimer = 0
minetest.register_globalstep(function(dtime)
    -- Reduce the amount of function calls
    dtimer = dtimer + dtime
    if dtimer < dtime_precision then
        return
    end

    for a=1, #active_arenas do
        local _, arena = arena_lib.get_arena_by_name("colour_jump", active_arenas[a])
        if arena then
            arena.seconds_left = arena.seconds_left - dtimer
            print_timer(arena)
        end
    end
    dtimer = 0
end)
